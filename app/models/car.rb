class Car < ActiveRecord::Base
  has_many :quotes
  validates_presence_of :model
  validates_presence_of :color
  validates_presence_of :vin
  validates_presence_of :retailprice
  before_save :calculate_markup

  def self.search(search)
    if search
      where('model LIKE ? OR color LIKE ? OR vin LIKE ?', "%#{search}%", "%#{search}%", "%#{search}%")
    else
      scoped
    end
  end

  def boolean_name
    carstatus ? 'Sold' : 'Unsold'
  end

  def car_details
    "#{model} #{color}"
  end

  def car_sold
    "#{model} #{color}, #{boolean_name}"
  end

  private
  def calculate_markup
   self.markupprice = retailprice + (retailprice * 0.1)
  end


end
