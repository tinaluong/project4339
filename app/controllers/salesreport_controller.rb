class SalesreportController < ApplicationController
  def index
    @quotes = Quote.where(:salestatus => true).includes(:salesperson).order("salespeople.last_name ASC, salespeople.first_name ASC")
  end
end
