class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :model
      t.string :color
      t.string :vin
      t.decimal :retailprice
      t.decimal :markupprice
      t.boolean :carstatus, default: false

      t.timestamps null: false
    end
  end
end
