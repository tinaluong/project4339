# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
updateLoan1 = ->
  quoteid = $('#loan_quote_id').val()
  $.getJSON '/quotes/' + quoteid + '/final_price', {},(json, response) ->

    total = json['final_price']

    downpayment = $('#loan_down_payment').val()

    loanamount = (total - downpayment).toFixed(2)

    $('#loan_loan_amount').val(loanamount)
    rate = $('#loan_interest_rate').val()
    term = $('#loan_term').val()
    r = rate / 1200
    numerator = r * loanamount
    denominator = 1 - (1 + r)**-(term*12)
    payment = (numerator/denominator).toFixed(2)

    $('#loan_monthly_payment').val(payment)


build_table = ->
  loan_amount = $('#loan_loan_amount').val()
  term = $('#loan_term').val()
  interest_rate = $("#loan_interest_rate").val()
  $.get "/loans/loan",{loan_amount: loan_amount,term: term, interes_rate: interest_rate}

$ ->
  $('#loan_quote_id, #loan_down_payment,#loan_term, #loan_interest_rate, #loan_monthly_payment').bind 'keyup mouseup mousewheel', -> updateLoan1()
  $('#loan_loan_amount,#loan_term, #loan_interest_rate').bind 'keyup mouseup mousewheel', ->
    build_table()

  build_table()




