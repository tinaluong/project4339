class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.decimal :down_payment
      t.decimal :loan_amount
      t.integer :term
      t.decimal :interest_rate
      t.decimal :monthly_payment
      t.integer :quote_id

      t.timestamps null: false
    end
  end
end
